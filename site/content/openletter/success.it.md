---
title: "Grazie per aver firmato!"
type: page
layout: subpage
---

## La tua firma è confermata

Grazie per aver firmato la lettera aperta "Denaro pubblico, codice pubblico". Il tuo supporto significa molto per noi. 

Se hai autorizzato la richiesta di avere il proprio nome aggiunto all'elenco pubblico, il tuo nome apparirà nell'[elenco di firme](../all-signatures) entro la prossima ora. Se ha autorizzato la richiesta di ricevere ulteriori informazioni, ti manterremo informato sulle notizie concernenti questa campagna via email.

## Prossimi passi

Per favore aiutaci dando alla tua firma grande impatto [condividendo questa campagna](../../#spread) con i tuoi amici. Assieme possiamo spingere chi prende le decisioni in politica e nelle pubbliche amministrazioni a rendere le licenze Software Libero/Open Source, per i nuovi software che verranno finanziati, la scelta predefinita nel settore pubblico in tutta Europa.

È anche possibile [ordinare adesivi e materiale informativo](https://fsfe.org/promo#pmpc) da Free Software Foundation Europe.

Rendi il Software Libero in Europa più forte e prendi in considerazione il [supporto a FSFE](https://fsfe.org/donate/?pmpc) o a una delle altre [organizzazioni di supporto](../../#organisations).

Con il tuo aiuto aiuteremo i centri decisionali di tutta Europa a capire che la pubblicazione del codice sotto licenze Software Libero/Open Source è la migliore soluzione per loro, per le altre amministrazioni, le società, e specialmente per il pubblico. 
